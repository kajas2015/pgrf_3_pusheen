#include "Source.h"

int main()
{
	Source source("PLANE",
		1620, 800,
		4, 4,
		false);

	while (!source.getWindowShouldClose())
	{
		source.update();
		source.render();
	}

	return 0;
}
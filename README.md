**PGRF-3-SEMESTRÁLNÍ PROJEKT**

**Karel Mistrík - cvičení 3.**

**Objekt reálného světa A1**

> Po spuštění projektu se nastaví kamera před připravené letadlo na kraji runwaye. Letadlo umí létat do všech stran pomocí šipek a kláves O a L, kterými se nastavuje jeho výška. WSAD Pak pohybuje s kamerou. Rozhlížení je řízeno myší.

Interakce s uživatelem ->
- WSAD -: pohyb kamerou
- Myš -: rozhlížení kamerou
- Šipky -: pohyb s letadlem do stran
- "L" -: letadlo nahoru
- "O" -: letadlo dolů
- Levé tlačítko myši -: nastavení nové pozice pro zdroj světla
